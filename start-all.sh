#!/bin/bash

container_installed() {
	return $(sudo docker container ls -a -f name=$1 | awk '(NR>1){print $2}' | wc -l)
}

container_id() {
	found=$(sudo docker container ls -a -f name=$1 | awk '(NR>1){print $1}')
	echo "(Started existing container) $found"
}

container_built() {
	return $(sudo docker images | grep $1 | wc -l)
}

container_name() {
	name=$(echo $1 | sed "s/^.*-name //g" | awk '{print $1}')
	echo $name
}

container_start() {
	docker_command=$1
	name=$(container_name "$docker_command")

	printf "%-35s" "$name:"

	container_built $name
	found=$?
	if [ $found -eq 0 ]; then
		echo "Not a valid built container."
	else
		container_installed $name
		found=$?
		if [[ $found == "1" ]]; then
			sudo docker start $name >> /dev/null
			container_id $name
		else
			sudo docker run $docker_command
		fi
	fi
}

# catalog-service
#
container_start "-d -p 8888:8080 --name catalog-service catalog-service"

