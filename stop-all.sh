#!/bin/bash

STOP_ONLY=0

show_help() {
	echo "Only supported options are:"
	echo " -h[?]: for help"
	echo " -s: to only stop images (do not remove their registration within docker)"
	echo "\n\n NOTE: By default docker images are stopped and removed from docker registration."
}

while getopts "h?s" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    s)  STOP_ONLY=1
        ;;
    esac
done

container_installed() {
	found=$(sudo docker container ls -a -f name=$1 | awk '(NR>1){print $2}')
	if [ -z "$found" ]; then
		return 0
	else
		return 1
	fi
}

container_id() {
	found=$(sudo docker container ls -a -f name=$1 | awk '(NR>1){print $1}')
	echo $found
}

container_stop() {
	printf "%-35s" "$1:"

	container_installed $1
	found=$?
	if [ $found -eq 1 ]; then
		container_id $1 1> /dev/null
		sudo docker stop $1 1> /dev/null
		echo -n "Stopped"

		if [ $STOP_ONLY -eq 0 ]; then
			sudo docker rm $1 1> /dev/null
			echo " and removed."
		else
			echo "."
		fi
	else
		echo "container not found!"
	fi
}

containers="catalog-service"
for i in $containers; do
	container_stop $i
done

