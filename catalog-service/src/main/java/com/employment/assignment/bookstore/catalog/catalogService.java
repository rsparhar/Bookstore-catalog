package com.employment.assignment.bookstore.catalog;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class catalogService {

	  @GET
	  @Path("/list")
	  @Produces(MediaType.TEXT_PLAIN)
	  public String readingList() {
	    return "Hello Jersey";
	  }

	  @GET
	  @Path("/view")
	  @Produces(MediaType.TEXT_HTML)
	  public String viewBook() {
	    return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey" + "</hello>";
	  }

	  @POST
	  @Path("/delete")
	  @Produces(MediaType.TEXT_HTML)
	  public String deleteBook() {
	    return "<html> " + "<title>" + "Hello Jersey" + "</title>"
	        + "<body><h1>" + "Hello Jersey" + "</body></h1>" + "</html> ";
	  }

	  @POST
	  @Path("/update")
	  @Produces(MediaType.TEXT_HTML)
	  public String updateBook() {
	    return "<html> " + "<title>" + "Hello Jersey" + "</title>"
	        + "<body><h1>" + "Hello Jersey" + "</body></h1>" + "</html> ";
	  }	  
}
